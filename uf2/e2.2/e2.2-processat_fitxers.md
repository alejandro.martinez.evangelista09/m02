# E2. Exercici 2. Processat de fitxers

## Introducció

Una de les grans ventatges de l'entorn UNIX és la facilitat per a processar fitxers, especialment fitxers amb dades, i transformar-les.

## Continguts

Feu servir el help i el manual per a cada comanda

### cut

La comanda cut ens permet extreure camps, per defecte separats per tabuladors, d'un llistat. Per exemple si tenim en un fitxer:

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
```

Amb la comanda ```cut -f 3 data.txt``` obtindrem:

```
three
gamma
```

que són els camps en tercera posició

### sort

Ens permetrà ordenar-los. Per exemple, si tenim:

```
apples
oranges
pears
kiwis
bananas
```

I fem:

```
sort data.txt
```

Obtindrem:

```
apples
bananas
kiwis
oranges
pears
```

### uniq

Ens permet filtrar línies repetides. Per exemple si tenim:

```
This is a line.
This is a line.
This is a line.
 
This is also a line.
This is also a line.
 
This is also also a line.
```

I executem **uniq data.txt** obtindrem:

```
This is a line.
 
This is also a line.
 
This is also also a line.
```

## Entrega

1. Llisteu els noms dels usuaris connectats al sistema.
2. Extraieu els camps 1 i 3 del resultat de l'ordre who.
3. Llisteu els permisos i el nom de tots els vostres fitxers situats en el directori HOME.
4. Llisteu el propietari i grandària de tots els fitxers del directori HOME.
5. Comproveu si existeix o no una entrada del vostre usuari en el fitxer /etc/passwd
6. Llisteu tots els usuaris del mateix grup que vosaltres que existeixin en aquest fitxer.
7. Llisteu els identificadors d'usuari
8. LListeu els shell usats pels usuaris.
9. Llisteu els camps 1 i 3 a 5.

Feu les següents preguntes amb el fitxer comma.dat

10. Ordena-ho per 'company'.
11. Ordena-ho per 'company' ignorant la diferència entre majúscules i minúscules i filtrant que surtin només els únics.
12. Llista els 'phone' ordenats alfabèticament.
13. Com s'ordenarien numèricament?
14. Presenta per ordre els telèfons majors que 1234567890.
15. Llista les grandàries i noms de tots els fitxers del directori ordenats per grandària (amb l'ordre sort)
16. Busqueu en el vostre directori HOME i en tots els seus subdirectoris els fitxers el nom dels quals contingui la lletra 'a'.
